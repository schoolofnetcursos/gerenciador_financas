<?php
/**
 * Created by PhpStorm.
 * User: LEONARDO
 * Date: 24/04/2017
 * Time: 08:53
 */

use LEOfin\Application;
use LEOfin\Plugins\RoutePlugin;
use LEOfin\ServiceContainer;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ServerRequestInterface;


require_once __DIR__ .'/../vendor/autoload.php';

$serviceContainer = new ServiceContainer();
$app = new Application($serviceContainer);

$app->plugin(new RoutePlugin());

$app->get('/', function(RequestInterface $request){
        $request->getUri();
    echo "oi";
});

$app->get('/home/{name}/{id}', function(ServerRequestInterface $request) {
    echo "Mostrando a Home";
    echo "<br/>";
    echo $request->getAttribute('name');
    echo "<br/>";
    echo $request->getAttribute('id');
});

$app->start();

