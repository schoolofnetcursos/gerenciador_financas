<?php

namespace LEOfin;

use Xtreamwayz\Pimple\Container;


class ServiceContainer implements ServiceContainerInterface {

	private $container;

    public function __construct()
    {
        $this->container = new Container();
    }

	public function add(string $name, $service) {

		$this->container[$name] = $service; // Armazenando serviço no container
	}


	public function addLazy(string $name, callable $callable) {

		$this->container[$name] = $this->container->factory($callable);
	}

	// Pegar o serviço
	public function get(string $name) {

	return $this->container->get($name); // 

	}

	// Verificar se o serviço existe
	public function has(string $name) {

	return $this->container->has($name);

	}

}