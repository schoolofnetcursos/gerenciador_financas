<?php 

namespace LEOfin\Plugins;

use LEOfin\ServiceContainerInterface;

interface PluginInterface {
	public function register(ServiceContainerInterface $container);


}