<?php 

declare(strict_types=1);
namespace LEOfin;

use LEOfin\Plugins\PluginInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ServerRequestInterface;

class Application {

	private $serviceContainer;

	public function __construct(serviceContainerInterface $serviceContainer) {
		$this->serviceContainer = $serviceContainer;
	}

	public function service($name) {
		return $this->serviceContainer->get($name);
	}
    // Void não faz nada, indica que a function n tem retorno
	public function addService(string $name, $service): void {
		if(is_callable($service)) {
			$this->serviceContainer->addLazy($name, $service);
		}

		else {
			$this->serviceContainer->add($name, $service);
		}
	}

	public function Plugin(PluginInterface $plugin): void {
	    $plugin->register($this->serviceContainer);
    }

    public function get($path, $action, $name = null): Application {
        $routing = $this->service('routing');
        $routing->get($name, $path, $action);
        return $this;
    }


    public function start() {
	    $route = $this->service('route');
	    /** @var ServerRequestInterface $request */
        $request = $this->service(RequestInterface::class);

        if(!$route){
            echo "Page not found";
            exit;
        }

        foreach ($route->attributes as $key => $value) {
            $request = $request->withAttribute($key, $value);
        }

        $request = $this->service(RequestInterface::class);
	    $callable = $route->handler;
	    $callable($request);
    }
}