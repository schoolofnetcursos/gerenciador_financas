<?php 
 // Sempre no topo do aqruivo, não é necessário inserir em todos os arquivos, somente quando for utilizar os type hinting do php > 7

declare(strict_types = 1);

namespace LEOfin;

interface ServiceContainerInterface {
	// Add service container
    public function add(string $name, $service);


    public function addLazy(string $name, callable $callable);




    // Pegar o serviço
    public function get(string $name);




	// Verificar se o serviço existe
    public function has(string $name);
}